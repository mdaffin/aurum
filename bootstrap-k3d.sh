#!/bin/bash
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
IFS=$'\n\t'

NAME=${1:-aurora}

if ! k3d registry list | grep k3d-aurora.localhost >/dev/null; then
    k3d registry create aurora.localhost --port 12345
fi
k3d cluster create -p "80:80@loadbalancer" --registry-use k3d-aurora.localhost:12345 "${NAME}"

kubectl apply -f https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
kubectl apply -f https://github.com/tektoncd/dashboard/releases/latest/download/tekton-dashboard-release.yaml
kubectl apply -f kube/minio.yaml
