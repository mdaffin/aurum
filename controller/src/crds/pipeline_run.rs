use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{
    api::{PostParams, Resource},
    Api, CustomResource,
};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::time::{SystemTime, UNIX_EPOCH};

use crate::Error;

use super::*;

#[derive(CustomResource, Debug, Clone, Deserialize, Serialize, JsonSchema)]
#[kube(group = "tekton.dev", version = "v1beta1", kind = "PipelineRun")]
#[kube(namespaced)]
#[kube(status = "PipelineRunStatus")]
pub struct PipelineRunSpec {
    #[serde(rename = "pipelineRef")]
    pub pipeline_ref: PipelineRunSpecPipelineRef,
    pub resources: Vec<PipelineRunSpecResource>,
}

#[derive(Debug, Clone, Deserialize, Serialize, JsonSchema)]
pub struct PipelineRunSpecPipelineRef {
    pub name: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, JsonSchema)]
pub struct PipelineRunSpecResource {
    pub name: String,
    #[serde(rename = "resourceRef")]
    pub resource_ref: PipelineRunSpecResourceRef,
}

#[derive(Debug, Clone, Deserialize, Serialize, JsonSchema)]
pub struct PipelineRunSpecResourceRef {
    pub name: String,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub struct PipelineRunStatus {
    pub conditions: Vec<PipelineRunStatusCondition>,
}

#[derive(Deserialize, Serialize, Clone, Debug, JsonSchema)]
pub struct PipelineRunStatusCondition {
    pub message: String,
    pub reason: String,
    pub status: String,
    #[serde(rename = "type")]
    pub ty: String,
}

pub async fn create_pipeline_run(client: kube::Client, metadata: &ObjectMeta) -> Result<(), Error> {
    let namespace = namespace_from_object_meta(metadata)?;
    let name = name_from_object_meta(metadata)?;

    let mut metadata = object_meta_from_generator_meta(metadata)?;

    metadata.name = Some(format!(
        "{}-{}",
        name,
        SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs()
    ));

    let pipeline = PipelineRun {
        api_version: "tekton.dev/v1beta1".to_string(),
        kind: PipelineRun::kind(&()).to_string(),
        metadata,
        spec: PipelineRunSpec {
            pipeline_ref: PipelineRunSpecPipelineRef {
                name: "aurora".to_string(),
            },
            resources: vec![PipelineRunSpecResource {
                name: "aur-repo".to_string(),
                resource_ref: PipelineRunSpecResourceRef {
                    name: name.to_string(),
                },
            }],
        },
        status: None,
    };

    Api::<PipelineRun>::namespaced(client.clone(), namespace)
        .create(&PostParams::default(), &pipeline)
        .await
        .map_err(|err| Error::JobCreationFailed { source: err })?;

    Ok(())
}
