use aurora::DbPath;
use color_eyre::{eyre::WrapErr, Report, Result};
use futures::StreamExt;
use itertools::Itertools;
use kube::{api::ListParams, Api};
use kube_runtime::controller::{Context, Controller, ReconcilerAction};
use std::str::FromStr;
use thiserror::Error;
use tokio::time::Duration;

mod crds;

use crds::archlinux_packages::*;
use crds::pipeline_resources::*;
use crds::pipeline_run::*;

#[derive(Error, Debug)]
pub enum Error {
    #[error("failed to create job: {}", source)]
    JobCreationFailed {
        #[from]
        source: kube::Error,
    },
    #[error("database error: {}", source)]
    Aurora {
        #[from]
        source: aurora::error::AuroraError,
    },
    #[error("database error: {}", source)]
    Database {
        #[from]
        source: aurora::database::DatabaseError,
    },
    #[error("database error: {}", source)]
    IoError {
        #[from]
        source: std::io::Error,
    },
    #[error("missing object key: {}", name)]
    MissingObjectKey { name: &'static str },
}

// Data we want access to in error/reconcile calls
struct Data {
    kube_client: kube::Client,
    s3_client: aws_sdk_s3::Client,
}

#[tokio::main]
async fn main() -> Result<()> {
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "info");
    }
    env_logger::init();
    color_eyre::install()?;

    let kube_client = kube::Client::try_default()
        .await
        .wrap_err_with(|| "failed to create kube client")?;
    let s3_client = aurora::create_s3_client(None, None, None)
        .await
        .wrap_err_with(|| "failed to create s3 client")?;
    bucket::create_bucket(&s3_client).await?;

    let alpkgs = Api::<ArchLinuxPackage>::all(kube_client.clone());
    let pipeline_resource_api = Api::<PipelineResource>::all(kube_client.clone());

    log::info!("starting archlinuxpackage-controller");

    Controller::new(alpkgs, ListParams::default())
        .owns(pipeline_resource_api, ListParams::default())
        .shutdown_on_signal()
        .run(
            reconcile,
            error_policy,
            Context::new(Data {
                kube_client,
                s3_client,
            }),
        )
        .for_each(|res| async move {
            match res {
                Ok(o) => log::info!("reconciled {:?}", o),
                Err(e) => log::warn!("reconcile failed: {}", Report::from(e)),
            }
        })
        .await;

    log::info!("controller terminated");
    Ok(())
}

/// Controller triggers this whenever our main object or our children changed
async fn reconcile(
    generator: ArchLinuxPackage,
    ctx: Context<Data>,
) -> Result<ReconcilerAction, Error> {
    log::info!("reconcile triggered");

    let s3_client = ctx.get_ref().s3_client.clone();

    let db_path = DbPath::from_str(&format!("s3://aurora/repo/x86_64/aurora.db.tar.xz"))?;
    let database = db_path.fetch_db(s3_client).await?;

    let contains_package = database
        .iter()?
        .filter_ok(|package| {
            Some(package.name.as_str()) == generator.package_name()
                && package.version == generator.package_version()
        })
        .next()
        .transpose()?
        .is_some();

    let kube_client = ctx.get_ref().kube_client.clone();
    let namespace = crds::namespace_from_object_meta(&generator.metadata)?;
    let name = crds::name_from_object_meta(&generator.metadata)?;

    let pipeline_runs = Api::<PipelineRun>::namespaced(kube_client.clone(), namespace)
        .list(&ListParams::default())
        .await
        .map_err(|err| Error::JobCreationFailed { source: err })?;

    let any_pipelines_running = pipeline_runs
        .iter()
        .filter(|&run| {
            run.metadata
                .name
                .as_deref()
                .map(|n| n.starts_with(name))
                .unwrap_or(false)
        })
        .filter_map(|run| run.status.as_ref())
        .map(|status| &status.conditions)
        .flatten()
        .map(|condition| &condition.reason)
        .any(|reason| reason == "Running");

    if !contains_package && !any_pipelines_running {
        create_pipeline_resource(kube_client.clone(), &generator.metadata).await?;
        create_pipeline_run(kube_client.clone(), &generator.metadata).await?;
    }

    Ok(ReconcilerAction {
        requeue_after: Some(Duration::from_secs(600)),
    })
}

/// The controller triggers this on reconcile errors
fn error_policy(_error: &Error, _ctx: Context<Data>) -> ReconcilerAction {
    ReconcilerAction {
        requeue_after: Some(Duration::from_secs(1)),
    }
}

impl ArchLinuxPackage {
    pub fn package_name(&self) -> Option<&str> {
        self.metadata.name.as_deref()
    }

    pub fn package_version(&self) -> &str {
        &self.spec.version
    }
}

impl Error {
    fn missing_object_key(name: &'static str) -> Error {
        Error::MissingObjectKey { name }
    }
}

mod bucket {
    use color_eyre::{eyre::WrapErr, Result};

    pub async fn create_bucket(client: &aws_sdk_s3::Client) -> Result<()> {
        let bucket = std::env::var("AURORA_BUCKET").wrap_err("failed to get env AURORA_BUCKET")?;
        match client.create_bucket().bucket(&bucket).send().await {
            Ok(_) => Ok(()),
            Err(aws_sdk_s3::SdkError::ServiceError {
                err:
                    aws_sdk_s3::error::CreateBucketError {
                        kind: aws_sdk_s3::error::CreateBucketErrorKind::BucketAlreadyOwnedByYou(..),
                        ..
                    },
                ..
            }) => {
                log::debug!("bucket already exists: {}", &bucket);
                Ok(())
            }
            Err(err) => Err(err.into()),
        }
    }
}
